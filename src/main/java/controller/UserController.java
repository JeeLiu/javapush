package controller;

import model.User;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * User: liuzhiliang
 * Date: 14-2-8
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */

@RequestMapping("/user")

public class UserController {
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User view(@PathVariable("id") Long id) {
        User user = new User();
        user.setId(id);
        user.setName("Liu");
        return user;
    }
}
