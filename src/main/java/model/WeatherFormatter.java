package model;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.logging.Logger;


/**
 * Created with IntelliJ IDEA.
 * User: liuzhiliang
 * Date: 14-2-13
 * Time: 15:36
 * To change this template use File | Settings | File Templates.
 */
public class WeatherFormatter {
    private static Logger log = Logger.getLogger(WeatherFormatter.class.toString());
    public String format( Weather weather ) throws Exception {
        log.info( "Formatting Weather Data" );
        Reader reader = new InputStreamReader( getClass().getClassLoader() .getResourceAsStream("output.vm"));
        VelocityContext context = new VelocityContext();
        context.put("weather", weather );
        StringWriter writer = new StringWriter();
        Velocity.evaluate(context, writer, "", reader);
        return writer.toString();
    }
}
