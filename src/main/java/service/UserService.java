package service;

import dao.UserDao;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: liuzhiliang
 * Date: 14-2-11
 * Time: 11:47
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(readOnly = true)
public class UserService {
    @Autowired
    private UserDao userDao;

    @Transactional(readOnly = false)
    public void save(User user) {
        this.userDao.saveOrUpdate(user);
    }

    public List<User> getUserList(Long id) {
        return this.userDao.queryUserInfo(id);
    }

    @Transactional(readOnly = false)
    public Integer deleteUserById(Long id) {
        return this.userDao.deleteById(id);
    }

    @Transactional(readOnly = false)
    public Integer deleteUser(User user) {
        List<User> list = this.userDao.queryUserInfo(user.getId());
        if (list != null && list.size() > 0) {
            this.userDao.delete(list.get(0));
        }
        return 1;
    }

}
