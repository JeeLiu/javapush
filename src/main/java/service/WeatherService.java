package service;

import model.Weather;
import model.YahooRetriever;
import model.WeatherFormatter;
import model.YahooParser;
import org.springframework.stereotype.Service;

import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: liuzhiliang
 * Date: 14-2-13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
@Service
public class WeatherService {
    public String retrieveForecast( String zip ) throws Exception { // Retrieve Data
        InputStream dataIn = new YahooRetriever().retrieve( zip );
// Parse Data
        Weather weather = new YahooParser().parse( dataIn );
        // Format (Print) Data
        return new WeatherFormatter().format( weather ); }
}
