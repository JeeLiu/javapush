package web;

import model.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: liuzhiliang
 * Date: 14-2-12
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
public interface ControllerInterface {

    public Integer saveUser(User user);

    public List<User> getUserList(Long id);

    public List<User> getUserListById(User user);

    public Integer deleteUserById(User user);

    public Integer deleteUser(User user);
}
