package web;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import service.UserService;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: XXXXXXXXXX
 * Date: 14-2-11
 * Time: 11:49
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class UserController implements ControllerInterface{

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/user/save")
    public String save(@RequestBody User user) {
        this.userService.save(user);
        return "True";
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "/user/saveUser" , method = RequestMethod.POST)
    public Integer saveUser(@RequestBody User user) {
        this.userService.save(user);
        return 1;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "/user/getUserList" , method = RequestMethod.POST)
    public List<User> getUserList(@RequestBody Long id) {
        return this.userService.getUserList(id);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "/user/getUserListById" , method = RequestMethod.POST)
    public List<User> getUserListById(@RequestBody User user){
        return this.userService.getUserList(user.getId());
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "/user/deleteUserById" , method = RequestMethod.POST)
    public Integer deleteUserById(User user) {
        return this.userService.deleteUserById(user.getId());  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "/user/deleteUser" , method = RequestMethod.POST)
    public Integer deleteUser(User user) {
        return this.userService.deleteUser(user);  //To change body of implemented methods use File | Settings | File Templates.
    }
}
