package dao;

import model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: liuzhiliang
 * Date: 14-2-11
 * Time: 11:40
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class UserDao extends GenericHibernateDao<User, Long> {
    public List<User> queryUserInfo(Long id) {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(User.class);

        if (id != null && id > 0) {
            detachedCriteria.add(Restrictions.eq("id", id));
        }

        List<User> list = this.findByDetachedCriteria(detachedCriteria);
        return list;
    }
}
